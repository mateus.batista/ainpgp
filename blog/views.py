import datetime

from django.core.mail import send_mail, EmailMessage
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy
from django.views.generic import ListView
from django.views.generic.edit import CreateView, UpdateView, DeleteView

# Create your views here.
from .forms import VantagemForm, OpiniaoForm, ComentarioForm, ContatoForm, AssocieForm
from .models import Vantagem, Opiniao, Banner, Publicacao, Comentario, Acesso, Colaborador, Diretoria, EstatutoDocumento
from ainpgp import settings




def index(request):
    vantagens = Vantagem.objects.all()
    opinioes = Opiniao.objects.all()
    banners = Banner.objects.all()
    Acesso.objects.create(data=datetime.datetime.now())
    context = {
        'vantagens': vantagens,
        'opinioes': opinioes,
        'banners': banners,
    }
    return render(request, 'index.html', context)


@login_required(login_url='login')
def admin_index(request):
    acessos = Acesso.objects.all().count()
    context = {
        'acessos': acessos
    }
    return render(request, 'admin-index.html', context)


def login_page(request):
    context = {}
    if request.user.is_authenticated:
        return redirect('admin_index')
    else:
        if request.method == 'POST':
            username = request.POST.get('username')
            password = request.POST.get('password')

            user = authenticate(request, username=username, password=password)

            if user is not None:
                login(request, user)
                return redirect('admin_index')
            else:
                messages.info(request, 'Usuário ou Senha inválidos')
    return render(request, 'login.html', context)


def logoutUser(request):
    logout(request)
    context = {}
    return redirect('login')


@login_required(login_url='login')
def vantagem_list(request):
    vantagens = Vantagem.objects.all()
    context = {
        'vantagens': vantagens,
    }
    return render(request, 'vantagem_list.html', context)


@login_required(login_url='login')
def vantagem_create(request):
    if request.method == "POST":
        form = VantagemForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('vantagem_list')
    else:
        form = VantagemForm()
        context = {
            'form': form
        }
    return render(request, 'vantagem_form.html', context)


def vantagem_edit(request, pk):
    vantagem = get_object_or_404(Vantagem, pk=pk)
    if request.method == "POST":
        form = VantagemForm(request.POST, instance=vantagem)
        if form.is_valid():
            form.save()
            return redirect('vantagem_list')
    else:
        form = VantagemForm(instance=vantagem)
        context = {
            'form': form
        }
    return render(request, 'vantagem_form.html', context)


def vantagem_delete(request, pk):
    vantagem = Vantagem.objects.get(pk=pk)
    vantagem.delete()
    return redirect('vantagem_list')


# Opiniões

@login_required(login_url='login')
def opiniao_list(request):
    opinioes = Opiniao.objects.all()
    context = {
        'opinioes': opinioes,
    }
    return render(request, 'opiniao_list.html', context)


@login_required(login_url='login')
def opiniao_create(request):
    if request.method == "POST":
        form = OpiniaoForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('opiniao_list')
    else:
        form = OpiniaoForm()
        context = {
            'form': form
        }
    return render(request, 'opiniao_form.html', context)


def opiniao_edit(request, pk):
    opiniao = get_object_or_404(Opiniao, pk=pk)
    if request.method == "POST":
        form = OpiniaoForm(request.POST, instance=opiniao)
        if form.is_valid():
            form.save()
            return redirect('opiniao_list')
    else:
        form = OpiniaoForm(instance=opiniao)
        context = {
            'form': form
        }
    return render(request, 'opiniao_form.html', context)


def opiniao_delete(request, pk):
    opiniao = Opiniao.objects.get(pk=pk)
    opiniao.delete()
    return redirect('opiniao_list')


def noticias_list(request):
    publicacoes = Publicacao.objects.all().order_by('-data')
    ultimas_publis = Publicacao.objects.all().order_by('-id')[:2]
    paginator = Paginator(publicacoes, 5)  # 5 posts in each page
    page = request.GET.get('page')

    try:
        publ_list = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer deliver the first page
        publ_list = paginator.page(1)
    except EmptyPage:
        # If page is out of range deliver last page of results
        publ_list = paginator.page(paginator.num_pages)

    context = {
        'publicacoes': publicacoes,
        'ultimas_publis': ultimas_publis,
        'page': page,
        'publ_list': publ_list,
    }
    return render(request, 'blog-list.html', context)


def noticia_detail(request, pk):
    publicacao = Publicacao.objects.get(pk=pk)
    ultimas_publis = Publicacao.objects.all().order_by('-id')[:2]
    comentarios = Comentario.objects.filter(publ=publicacao)
    if request.method == "POST":
        form = ComentarioForm(request.POST)
        if form.is_valid():
            comentario = form.save(commit=False)
            comentario.data = datetime.datetime.now()
            comentario.publ = publicacao
            comentario.save()
            return redirect('noticias_list')
    else:
        form = ComentarioForm()
        context = {
            'publicacao': publicacao,
            'ultimas_publis': ultimas_publis,
            'comentarios': comentarios,
            'form': form
        }
        return render(request, 'post-detail.html', context)


class AtualidadeListView(ListView, LoginRequiredMixin):
    model = Publicacao
    paginate_by = 100
    template_name = 'atualidade_list.html'


class AtualidadeCreateView(CreateView, LoginRequiredMixin):
    model = Publicacao
    fields = ['titulo', 'texto', 'imagem', 'autor', 'categoria']
    template_name = 'noticia_form.html'
    success_url = reverse_lazy('atualidade_list')


class AtualidadeUpdateView(UpdateView, LoginRequiredMixin):
    model = Publicacao
    fields = ['titulo', 'texto', 'imagem', 'autor', 'categoria']
    template_name = 'noticia_form.html'
    success_url = reverse_lazy('atualidade_list')


class AtualidadeDeleteView(DeleteView, LoginRequiredMixin):
    model = Publicacao
    success_url = reverse_lazy('atualidade_list')


def contato(request):
    if request.method == "POST":
        form = ContatoForm(request.POST)
        if form.is_valid():
            cd = form.cleaned_data
            subject = "Contato AINPGP"
            cabecalho = '<p>Esta mensagem foi enviada por {} através da página de contato</p><br/>'.format(cd['nome'])
            email_enviado = cd['email']
            message = cabecalho + email_enviado + '<br/>' + cd['mensagem']

            # send the email to the recipent
            msg = EmailMessage(subject, message,
                               settings.DEFAULT_FROM_EMAIL, [settings.DEFAULT_FROM_EMAIL])

            msg.content_subtype = "html"  # Main content is now text/html
            msg.send()

            # set the variable initially created to True
            messageSent = True
            contatoform = ContatoForm()
            context = {
                'messageSent': messageSent,
                'contatoform': contatoform
            }
            return render(request, 'contato.html', context)
    else:
        contatoform = ContatoForm()
        context = {
            'contatoform': contatoform
        }
        return render(request, 'contato.html', context)

class ColaboradorListView(ListView, LoginRequiredMixin):
    model = Colaborador
    template_name = 'colaborador_list.html'

class ColaboradorCreateView(CreateView, LoginRequiredMixin):
    model = Colaborador
    fields = ['nome', 'email', 'telefone', 'ativo']
    template_name = 'colaborador_form.html'
    success_url = reverse_lazy('colaborador_list')

class ColaboradorUpdateView(UpdateView, LoginRequiredMixin):
    model = Colaborador
    fields = ['nome', 'email', 'telefone', 'ativo']
    template_name = 'colaborador_form.html'
    success_url = reverse_lazy('colaborador_list')

class ColaboradorDeleteView(DeleteView, LoginRequiredMixin):
    model = Colaborador
    success_url = reverse_lazy('colaborador_list')

def quem_somos(request):
    colaboradores = Colaborador.objects.all()
    context = {
        'colaboradores': colaboradores
    }
    return render(request, 'quem_somos.html', context)

class DiretoriaListView(ListView, LoginRequiredMixin):
    model = Diretoria
    template_name = 'diretoria_list.html'

class DiretoriaCreateView(CreateView, LoginRequiredMixin):
    model = Diretoria
    fields = ['nome', 'sigla', 'cargo']
    template_name = 'diretoria_form.html'
    success_url = reverse_lazy('diretoria_list')

class DiretoriaUpdateView(UpdateView, LoginRequiredMixin):
    model = Diretoria
    fields = ['nome', 'sigla', 'cargo']
    template_name = 'diretoria_form.html'
    success_url = reverse_lazy('diretoria_list')

class DiretoriaDeleteView(DeleteView, LoginRequiredMixin):
    model = Diretoria
    success_url = reverse_lazy('diretoria_list')

def diretoria(request):
    diretores = Diretoria.objects.all()
    context = {
        'diretores': diretores
    }
    return render(request, 'diretoria.html', context)


class EstatutoDocumentoListView(ListView, LoginRequiredMixin):
    model = EstatutoDocumento
    template_name = 'estatuto_documento_list.html'

class EstatutoDocumentoCreateView(CreateView, LoginRequiredMixin):
    model = EstatutoDocumento
    fields = ['titulo', 'descricao', 'arquivo']
    template_name = 'estatuto_documento_form.html'
    success_url = reverse_lazy('estatuto_documento_list')

class EstatutoDocumentoUpdateView(UpdateView, LoginRequiredMixin):
    model = EstatutoDocumento
    fields = ['titulo', 'descricao', 'arquivo']
    template_name = 'estatuto_documento_form.html'
    success_url = reverse_lazy('estatuto_documento_list')

class EstatutoDocumentoDeleteView(DeleteView, LoginRequiredMixin):
    model = EstatutoDocumento
    success_url = reverse_lazy('estatuto_documento_list')

def associados(request):
    if request.method == "POST":
        form = AssocieForm(request.POST)
        if form.is_valid():
            # set the variable initially created to True
            messageSent = True
            associeform = AssocieForm()
            context = {
                'messageSent': messageSent,
                'associeform': associeform
            }
            return render(request, 'associe.html', context)
    else:
        associeform = AssocieForm()
        context = {
            'associeform': associeform
        }
        return render(request, 'associe.html', context)