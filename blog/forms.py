from django import forms
from django.forms import ModelForm

from .models import Vantagem, Opiniao, Comentario, Associacao


class VantagemForm(forms.ModelForm):
    class Meta:
        model = Vantagem
        fields = '__all__'

class OpiniaoForm(forms.ModelForm):
    class Meta:
        model = Opiniao
        fields = '__all__'

class ComentarioForm(forms.ModelForm):
    class Meta:
        model = Comentario
        fields = [
            'comentario',
            'nome',
            'publ',
                  ]

class ContatoForm(forms.Form):
    email = forms.EmailField()
    nome = forms.CharField()
    motivo_contato = forms.CharField()
    mensagem = forms.CharField(widget=forms.Textarea)

class AssocieForm(ModelForm):
    class Meta:
        model = Associacao
        fields = '__all__'