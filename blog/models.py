from django.conf import settings
from django.db import models


# Create your models here.


class Autor(models.Model):
    user = models.OneToOneField(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    bio = models.CharField(max_length=240, blank=True)

    def __str__(self):
        return self.user.get_username()




class Publicacao(models.Model):
    CATEGORIAS = [
        ('NT', 'Notícias'),
        ('OP', 'Opinião'),
        ('EX', 'Experiências'),
    ]

    titulo = models.CharField(max_length=255)
    texto = models.TextField()
    imagem = models.ImageField(upload_to='images')
    autor = models.ForeignKey(Autor, on_delete=models.CASCADE)
    data = models.DateField(db_index=True, auto_now_add=True)
    categoria = models.CharField(choices=CATEGORIAS, max_length=2)

    def __str__(self):
        return self.titulo


class Comentario(models.Model):
    comentario = models.CharField(max_length=400)
    nome = models.CharField(max_length=150)
    publ = models.ForeignKey(Publicacao, on_delete=models.CASCADE, blank=True)
    data = models.DateField(db_index=True, auto_now_add=True, blank=True)

    def __str__(self):
        return self.comentario


class Vantagem(models.Model):
    titulo = models.CharField(max_length=35)
    texto = models.CharField(max_length=240)

    def __str__(self):
        return self.titulo


class Opiniao(models.Model):
    texto = models.TextField(max_length=350)
    nome = models.CharField(max_length=100)
    vinculo = models.CharField(max_length=100)

    def __str__(self):
        return self.texto


class Banner(models.Model):
    titulo = models.CharField(max_length=50)
    texto = models.CharField(max_length=150)
    link = models.URLField()

    def __str__(self):
        return self.titulo


class Acesso(models.Model):
    data = models.DateTimeField(auto_now_add=True)


class Colaborador(models.Model):
    nome = models.CharField(max_length=250)
    email = models.EmailField()
    telefone = models.CharField(max_length=13)
    ativo = models.DateField()

class Diretoria(models.Model):
    nome = models.CharField(max_length=250, verbose_name='Título/Nome')
    sigla = models.CharField(max_length=10, verbose_name='Sigla da Instituição')
    cargo = models.CharField(max_length=50)

class EstatutoDocumento(models.Model):
    titulo = models.CharField(max_length=250, verbose_name='Título')
    descricao = models.CharField(max_length=500, verbose_name='Descrição')
    arquivo = models.FileField()

class Imagens(models.Model):
    imagem = models.ImageField()

class InformesFIPED(models.Model):
    titulo = models.CharField(max_length=250, verbose_name='Título')
    texto = models.TextField(verbose_name='Texto', blank=True, null=True)
    imagens = models.ManyToManyField(Imagens, blank=True, null=True)

class EdicoesFIPED(models.Model):
    titulo = models.CharField(max_length=250, verbose_name='Título')
    capa = models.ImageField()
    texto = models.TextField(verbose_name='Texto', blank=True, null=True)
    imagens = models.ManyToManyField(Imagens, blank=True, null=True)

class Associacao(models.Model):

    ATIVIDADE_CHOICES = [
        ('PPNS','Professores, pesquisadores de nível superior'),
        ('PEB','Professores da Educação Básica'),
        ('PE','Profissionais da educação'),
        ('EPG','Estudantes de Pós Graduação'),
        ('EG','Estudantes de Graduação e outras categorias'),
    ]
    PAGAMENTO_CHOICES = [
        ('PP','Paypal'),
        ('T','Transferência'),
    ]

    fisica = models.BooleanField(blank=True)
    juridica = models.BooleanField(blank=True)
    nome_razao = models.CharField(max_length=255)
    data_nascimento = models.DateField()
    cpf_cnpj = models.CharField(max_length=14)
    endereco = models.CharField(max_length=255)
    numero_end = models.CharField(max_length=5)
    cep = models.CharField(max_length=8)
    bairro = models.CharField(max_length=100)
    cidade = models.CharField(max_length=100)
    uf = models.CharField(max_length=2)
    pais = models.CharField(max_length=50)
    tel_cel = models.CharField(max_length=12)
    tel_fixo = models.CharField(max_length=12)
    email = models.EmailField()
    endereco_trab = models.CharField(max_length=255)
    numero_end_trab = models.CharField(max_length=5)
    cep_trab = models.CharField(max_length=8)
    bairro_trab = models.CharField(max_length=100)
    cidade_trab = models.CharField(max_length=100)
    uf_trab = models.CharField(max_length=2)
    pais_trab = models.CharField(max_length=50)
    tel_trab = models.CharField(max_length=12)
    email_trab = models.EmailField()
    lattes = models.URLField()
    ensino_fundamental = models.CharField(max_length=255)
    ensino_medio = models.CharField(max_length=255)
    graduacao = models.CharField(max_length=255)
    especializacao = models.CharField(max_length=255)
    mestrado = models.CharField(max_length=255)
    doutorado = models.CharField(max_length=255)
    pos_doc = models.CharField(max_length=255)
    outro = models.CharField(max_length=255)
    atividade_prof = models.CharField(max_length=4, choices=ATIVIDADE_CHOICES)
    pagamento = models.CharField(max_length=4, choices=PAGAMENTO_CHOICES)


