from django.contrib import admin
from .models import *

# Register your models here.

admin.site.register(Publicacao)
admin.site.register(Autor)
admin.site.register(Comentario)
admin.site.register(Vantagem)
admin.site.register(Opiniao)
admin.site.register(Banner)
