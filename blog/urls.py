from django.urls import path

from .views import *

urlpatterns = [
    path('', index, name='index'),
    # Admin
    path('admin_index/', admin_index, name='admin_index'),
    # Login
    path('login/', login_page, name='login'),
    path('logout/', logoutUser, name='logout'),
    # Vantagem
    path('vantagem_list/', vantagem_list, name='vantagem_list'),
    path('vantagem_create/', vantagem_create, name='vantagem_create'),
    path('vantagem_edit/<int:pk>/', vantagem_edit, name='vantagem_edit'),
    path('vantagem_delete/<int:pk>/', vantagem_delete, name='vantagem_delete'),
    # Opinioes
    path('opiniao_list/', opiniao_list, name='opiniao_list'),
    path('opiniao_create/', opiniao_create, name='opiniao_create'),
    path('opiniao_edit/<int:pk>/', opiniao_edit, name='opiniao_edit'),
    path('opiniao_delete/<int:pk>/', opiniao_delete, name='opiniao_delete'),
    # Atualidades
    path('atualidades/', noticias_list, name='noticias_list'),
    path('atualidades/<int:pk>', noticia_detail, name='post-detail'),
    path('atualidades_list/', AtualidadeListView.as_view(), name='atualidade_list'),
    path('atualidades_create/', AtualidadeCreateView.as_view(), name='atualidade_create'),
    path('atualidades_update/<int:pk>/', AtualidadeUpdateView.as_view(), name='atualidade_update'),
    path('atualidades_delete/<int:pk>/', AtualidadeDeleteView.as_view(), name='atualidade_delete'),
    # Contato
    path('contato/', contato, name='contato'),
    # Quem Somos
    path('quem_somos/', quem_somos, name='quem_somos'),
    # Colaborador
    path('colaborador_list/', ColaboradorListView.as_view(), name='colaborador_list'),
    path('colaborador_create/', ColaboradorCreateView.as_view(), name='colaborador_create'),
    path('colaborador_update/<int:pk>/', ColaboradorUpdateView.as_view(), name='colaborador_edit'),
    path('colaborador_delete/<int:pk>/', ColaboradorDeleteView.as_view(), name='colaborador_delete'),
    # Diretoria
    path('diretoria/', diretoria, name='diretoria'),
    path('diretoria_list/', DiretoriaListView.as_view(), name='diretoria_list'),
    path('diretoria_create/', DiretoriaCreateView.as_view(), name='diretoria_create'),
    path('diretoria_update/<int:pk>/', DiretoriaUpdateView.as_view(), name='diretoria_edit'),
    path('diretoria_delete/<int:pk>/', DiretoriaDeleteView.as_view(), name='diretoria_delete'),
    # Estatuto e documentos
    #path('diretoria/', diretoria, name='diretoria'),
    path('estatuto_documento_list/', EstatutoDocumentoListView.as_view(), name='estatuto_documento_list'),
    path('estatuto_documento_create/', EstatutoDocumentoCreateView.as_view(), name='estatuto_documento_create'),
    path('estatuto_documento_update/<int:pk>/', EstatutoDocumentoUpdateView.as_view(), name='estatuto_documento_edit'),
    path('estatuto_documento_delete/<int:pk>/', EstatutoDocumentoDeleteView.as_view(), name='estatuto_documento_delete'),
    # Formulario Associados
    path('associe/', associados, name='associe'),

]
